const cb = require("./test/testFind");

const find =(elements,cb,index1)=>{
    let a=0;
    if(!elements || !cb ) return [];
    if(index1)a= index1;
    for (let i=a;i< elements.length; i++){
        let b =cb(elements[i]);
        if (b) return b;
    }
    return undefined;
};


module.exports=find;
