const cb = require("./test/testMap");

const map =(elements,cb)=>{
    if(!elements || !cb ) return [];
    const newArr= [];

    for (let i=0;i< elements.length; i++){
        newArr.push(cb(elements[i]));
    }
    return newArr;
}


module.exports=map;
