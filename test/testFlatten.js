const flatten = require("../flatten")

const nestedArray = [1,true,['javasript',['hello world']],[[1,[2]]]];

const result = flatten(nestedArray);
console.log(result);
